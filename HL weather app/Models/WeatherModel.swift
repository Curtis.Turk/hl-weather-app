//
//  Weather.swift
//  HL weather app
//
//  Created by Curtis Turk on 17/07/2023.
//

import Foundation

struct WeatherModel: Codable {
    let latitude: Double
    let longitude: Double
    let generationtimeMs: Double
    let utcOffsetSeconds: Int
    let timezone: String
    let timezoneAbbreviation: String
    let elevation: Double
    
    let hourlyUnits: HourlyUnits?
    let hourly: Hourly?
    
    let dailyUnits: DailyUnits?
    let daily: Daily?
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude
        case generationtimeMs = "generationtime_ms"
        case utcOffsetSeconds = "utc_offset_seconds"
        case timezone
        case timezoneAbbreviation = "timezone_abbreviation"
        case elevation
        case hourlyUnits = "hourly_units"
        case hourly
        case dailyUnits = "daily_units"
        case daily
    }
    
    struct HourlyUnits: Codable {
        let time: String
        let temperature2m: String
        let precipitationProbability: String
        let precipitation: String
        let windspeed10m: String
        let winddirection10m: String
        let weathercode: String
        
        enum CodingKeys: String, CodingKey {
            case time
            case temperature2m = "temperature_2m"
            case precipitationProbability = "precipitation_probability"
            case precipitation
            case windspeed10m = "windspeed_10m"
            case winddirection10m = "winddirection_10m"
            case weathercode
        }
    }


    struct Hourly: Codable {
        let time: [String]
        let temperature2m: [Double]
        let precipitationProbability: [Int?]
        let precipitation: [Double]
        let windspeed10m: [Double]
        let winddirection10m: [Int]
        let weathercode: [Int]
        
        enum CodingKeys: String, CodingKey {
            case time
            case temperature2m = "temperature_2m"
            case precipitationProbability = "precipitation_probability"
            case precipitation
            case windspeed10m = "windspeed_10m"
            case winddirection10m = "winddirection_10m"
            case weathercode
        }
    }

    struct DailyUnits: Codable {
        let time: String
        let temperature2mMax: String
        let temperature2mMin: String
        let precipitationSum: String
        let windspeed10mMax: String
        let winddirection10mDominant: String
        let weathercode: String
        
        enum CodingKeys: String, CodingKey {
            case time
            case temperature2mMax = "temperature_2m_max"
            case temperature2mMin = "temperature_2m_min"
            case precipitationSum = "precipitation_sum"
            case windspeed10mMax = "windspeed_10m_max"
            case winddirection10mDominant = "winddirection_10m_dominant"
            case weathercode
        }
    }

    struct Daily: Codable {
        let time: [String]
        let temperature2mMax: [Double]
        let temperature2mMin: [Double]
        let precipitationSum: [Double]
        let windspeed10mMax: [Double]
        let winddirection10mDominant: [Int]
        let weathercode: [Int]
        
        enum CodingKeys: String, CodingKey {
            case time
            case temperature2mMax = "temperature_2m_max"
            case temperature2mMin = "temperature_2m_min"
            case precipitationSum = "precipitation_sum"
            case windspeed10mMax = "windspeed_10m_max"
            case winddirection10mDominant = "winddirection_10m_dominant"
            case weathercode
        }
    }
}

extension WeatherModel {
    struct StubFactory {
        public static func makeDailyWeatherUnits(
            time: String = "iso8601",
            temperature2mMax: String = "°C",
            temperature2mMin: String = "°C",
            precipitationSum: String =  "mm",
            windspeed10mMax: String = "km/h",
            winddirection10mDominant: String = "°",
            weathercode: String = "wmo code"
        ) -> DailyUnits {
            return DailyUnits(
                time: time,
                temperature2mMax: temperature2mMax,
                temperature2mMin: temperature2mMin,
                precipitationSum: precipitationSum,
                windspeed10mMax: windspeed10mMax,
                winddirection10mDominant: winddirection10mDominant,
                weathercode: weathercode
            )
        }
        
        public static func makeDailyWeather(
            time: [String] =  ["2023-07-26","2023-07-27","2023-07-28","2023-07-29","2023-07-30","2023-07-31","2023-08-01"],
            temperature2mMax: [Double] = [19.7,21.2,23.4,22.6,23.6,22.0,22.0],
            temperature2mMin: [Double] = [12.1,12.0,15.7,16.4,16.0,15.6,15.6],
            precipitationSum: [Double] = [6.60,2.40,5.50,6.20,0.00,0.00,9.00],
            windspeed10mMax: [Double] = [14.1,15.3,16.6,13.5,14.6,13.6,11.8],
            winddirection10mDominant: [Int] = [276,204,252,206,253,233,186],
            weathercode: [Int] = [80,80,3,3,3,61,3]
        ) -> Daily {
            return Daily(
                time: time,
                temperature2mMax: temperature2mMax,
                temperature2mMin: temperature2mMin,
                precipitationSum: precipitationSum,
                windspeed10mMax: windspeed10mMax,
                winddirection10mDominant: winddirection10mDominant,
                weathercode: weathercode
            )
        }
        
        public static func makeDailyWeatherModel(
            latitude: Double = 52.52,
            longitude: Double = 13.419998,
            generationtimeMs: Double = 0.7989406585693359,
            utcOffsetSeconds: Int = 7200,
            timezone: String = "Europe/Berlin",
            timezoneAbbreviation: String = "CEST",
            elevation: Double = 38.0,
            hourlyUnits: HourlyUnits? = nil,
            hourly: Hourly? = nil,
            dailyUnits: DailyUnits? = makeDailyWeatherUnits(),
            daily: Daily? = makeDailyWeather()
        ) -> WeatherModel {
            return WeatherModel(
                latitude: latitude,
                longitude: longitude,
                generationtimeMs: generationtimeMs,
                utcOffsetSeconds: utcOffsetSeconds,
                timezone: timezone,
                timezoneAbbreviation: timezoneAbbreviation,
                elevation: elevation,
                hourlyUnits: hourlyUnits,
                hourly: hourly,
                dailyUnits: dailyUnits,
                daily: daily
            )
        }
        
        public static func makeHourlyWeatherUnits(
            time: String = "iso8601",
            temperature2m: String = "°C",
            precipitationProbability: String = "%",
            precipitation: String = "mm",
            windspeed10m: String = "km/h",
            winddirection10m: String = "°",
            weathercode: String = "wmo code"
        ) -> HourlyUnits {
            return HourlyUnits(
                time: time,
                temperature2m: temperature2m,
                precipitationProbability: precipitationProbability,
                precipitation: precipitation,
                windspeed10m: windspeed10m,
                winddirection10m: winddirection10m,
                weathercode: weathercode
            )
        }
        
        public static func makeHourlyWeather(
            time: [String] =  ["2023-07-31T00:00","2023-07-31T01:00","2023-07-31T02:00","2023-07-31T03:00","2023-07-31T04:00","2023-07-31T05:00","2023-07-31T06:00","2023-07-31T07:00","2023-07-31T08:00","2023-07-31T09:00","2023-07-31T10:00","2023-07-31T11:00","2023-07-31T12:00","2023-07-31T13:00","2023-07-31T14:00","2023-07-31T15:00","2023-07-31T16:00","2023-07-31T17:00","2023-07-31T18:00","2023-07-31T19:00","2023-07-31T20:00","2023-07-31T21:00","2023-07-31T22:00","2023-07-31T23:00","2023-08-01T00:00","2023-08-01T01:00","2023-08-01T02:00","2023-08-01T03:00","2023-08-01T04:00","2023-08-01T05:00","2023-08-01T06:00","2023-08-01T07:00","2023-08-01T08:00","2023-08-01T09:00","2023-08-01T10:00","2023-08-01T11:00","2023-08-01T12:00","2023-08-01T13:00","2023-08-01T14:00","2023-08-01T15:00","2023-08-01T16:00","2023-08-01T17:00","2023-08-01T18:00","2023-08-01T19:00","2023-08-01T20:00","2023-08-01T21:00","2023-08-01T22:00","2023-08-01T23:00","2023-08-02T00:00","2023-08-02T01:00","2023-08-02T02:00","2023-08-02T03:00","2023-08-02T04:00","2023-08-02T05:00","2023-08-02T06:00","2023-08-02T07:00","2023-08-02T08:00","2023-08-02T09:00","2023-08-02T10:00","2023-08-02T11:00","2023-08-02T12:00","2023-08-02T13:00","2023-08-02T14:00","2023-08-02T15:00","2023-08-02T16:00","2023-08-02T17:00","2023-08-02T18:00","2023-08-02T19:00","2023-08-02T20:00","2023-08-02T21:00","2023-08-02T22:00","2023-08-02T23:00","2023-08-03T00:00","2023-08-03T01:00","2023-08-03T02:00","2023-08-03T03:00","2023-08-03T04:00","2023-08-03T05:00","2023-08-03T06:00","2023-08-03T07:00","2023-08-03T08:00","2023-08-03T09:00","2023-08-03T10:00","2023-08-03T11:00","2023-08-03T12:00","2023-08-03T13:00","2023-08-03T14:00","2023-08-03T15:00","2023-08-03T16:00","2023-08-03T17:00","2023-08-03T18:00","2023-08-03T19:00","2023-08-03T20:00","2023-08-03T21:00","2023-08-03T22:00","2023-08-03T23:00","2023-08-04T00:00","2023-08-04T01:00","2023-08-04T02:00","2023-08-04T03:00","2023-08-04T04:00","2023-08-04T05:00","2023-08-04T06:00","2023-08-04T07:00","2023-08-04T08:00","2023-08-04T09:00","2023-08-04T10:00","2023-08-04T11:00","2023-08-04T12:00","2023-08-04T13:00","2023-08-04T14:00","2023-08-04T15:00","2023-08-04T16:00","2023-08-04T17:00","2023-08-04T18:00","2023-08-04T19:00","2023-08-04T20:00","2023-08-04T21:00","2023-08-04T22:00","2023-08-04T23:00","2023-08-05T00:00","2023-08-05T01:00","2023-08-05T02:00","2023-08-05T03:00","2023-08-05T04:00","2023-08-05T05:00","2023-08-05T06:00","2023-08-05T07:00","2023-08-05T08:00","2023-08-05T09:00","2023-08-05T10:00","2023-08-05T11:00","2023-08-05T12:00","2023-08-05T13:00","2023-08-05T14:00","2023-08-05T15:00","2023-08-05T16:00","2023-08-05T17:00","2023-08-05T18:00","2023-08-05T19:00","2023-08-05T20:00","2023-08-05T21:00","2023-08-05T22:00","2023-08-05T23:00","2023-08-06T00:00","2023-08-06T01:00","2023-08-06T02:00","2023-08-06T03:00","2023-08-06T04:00","2023-08-06T05:00","2023-08-06T06:00","2023-08-06T07:00","2023-08-06T08:00","2023-08-06T09:00","2023-08-06T10:00","2023-08-06T11:00","2023-08-06T12:00","2023-08-06T13:00","2023-08-06T14:00","2023-08-06T15:00","2023-08-06T16:00","2023-08-06T17:00","2023-08-06T18:00","2023-08-06T19:00","2023-08-06T20:00","2023-08-06T21:00","2023-08-06T22:00","2023-08-06T23:00"],
            temperature2m: [Double] = [17.0,17.2,17.0,17.5,17.4,17.4,17.3,17.6,18.0,18.0,17.8,17.6,17.4,17.0,17.2,18.3,18.3,18.3,17.8,17.1,16.8,16.6,16.6,16.6,16.4,16.2,16.0,15.6,15.0,14.5,14.8,15.6,16.5,17.4,18.3,18.9,19.4,19.6,19.7,19.7,19.8,19.4,19.0,16.9,16.5,16.0,15.0,14.5,14.9,16.2,16.8,17.0,16.7,15.5,15.2,15.5,15.7,16.1,15.8,15.6,16.2,16.6,16.6,16.6,17.3,17.5,17.3,16.6,15.8,15.5,14.9,14.5,14.9,14.9,14.7,14.5,14.3,14.2,14.4,15.4,15.3,16.0,16.5,17.2,17.6,17.6,17.7,18.2,18.0,18.1,17.5,16.8,15.9,15.5,15.3,14.9,14.5,14.2,14.0,14.0,14.0,13.8,13.9,13.7,14.7,15.8,17.0,18.2,19.0,19.2,19.0,18.8,18.5,18.2,17.7,17.0,16.1,15.4,15.0,14.7,14.5,14.2,13.9,13.5,13.1,12.6,12.2,14.0,14.5,14.8,15.1,15.2,15.4,15.5,15.7,15.9,16.2,16.7,16.8,16.4,15.7,15.0,14.6,14.2,13.8,13.5,13.2,12.9,12.6,12.2,12.3,13.2,14.6,15.8,16.7,17.4,17.9,18.1,18.2,18.0,17.8,17.4,17.0,16.5,16.0,15.5,15.0,14.5],
            precipitationProbability: [Int] =  [100,100,100,100,71,42,13,9,4,0,23,45,68,58,49,39,34,28,23,21,18,16,19,23,26,21,15,10,8,5,3,2,1,0,2,4,6,7,9,10,10,10,10,30,51,71,81,90,100,99,98,97,89,82,74,82,89,97,98,99,100,100,100,100,100,100,100,90,81,71,52,32,13,18,24,29,31,33,35,42,48,55,61,68,74,76,79,81,71,62,52,39,26,13,9,4,0,0,0,0,0,0,0,1,2,3,16,29,42,43,44,45,42,38,35,24,14,3,10,16,23,27,31,35,35,35,35,38,42,45,56,66,77,76,75,74,65,57,48,38,29,19,17,15,13,16,20,23,22,20,19,22,26,29,32,36,39,45,52,58,54,49,45,35,26,16,16,16],
            precipitation: [Double] =  [0.00,0.10,0.00,0.20,0.00,0.10,0.20,0.00,0.00,0.00,0.00,0.00,0.00,0.50,0.30,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1.30,2.50,2.30,1.10,0.30,0.30,0.20,0.10,0.00,0.00,0.10,0.00,0.00,0.40,1.40,1.30,0.80,0.80,0.50,0.20,0.30,0.20,1.20,0.70,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.20,0.30,0.10,0.00,0.00,0.10,0.20,0.20,0.10,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.10,0.10,0.10,1.70,1.70,1.70,0.00,0.00,0.00,0.10,0.10,0.10,0.10,0.10,0.10,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.20,0.20,0.20,0.30,0.30,0.30,0.00,0.00,0.00,0.00,0.00],
            windspeed10m: [Double] = [18.1,18.1,15.0,18.0,20.1,20.6,14.8,14.4,15.9,18.7,18.1,14.6,14.1,15.2,23.8,25.6,23.5,22.7,21.7,21.7,22.0,20.1,18.7,18.9,17.4,16.2,18.4,16.6,16.6,14.5,14.4,17.3,16.2,18.7,18.1,18.5,20.0,21.8,22.1,19.7,15.9,13.3,10.0,6.6,7.3,19.2,20.5,22.3,26.4,24.1,22.2,19.6,25.7,21.7,21.1,22.9,23.7,23.1,24.9,27.8,25.5,23.7,22.4,25.3,27.7,29.1,22.0,25.5,19.6,19.2,17.7,17.0,13.3,13.6,13.5,12.5,11.5,10.7,12.6,19.1,16.8,15.0,14.6,14.7,14.0,15.6,15.3,21.9,26.7,23.0,21.7,19.6,19.4,18.8,17.2,16.3,15.5,16.6,16.3,16.2,17.3,15.1,13.0,9.0,12.0,14.6,15.5,15.3,15.2,15.3,15.3,15.6,14.8,13.4,11.6,8.7,5.4,4.3,4.1,3.6,3.6,4.8,6.0,6.5,7.2,11.0,14.8,2.9,2.9,2.5,1.9,1.6,1.1,1.6,5.2,7.5,8.4,9.2,9.7,9.0,7.6,6.3,5.4,4.5,4.0,3.6,3.3,2.9,2.5,2.5,2.5,3.0,4.4,6.1,7.5,9.2,10.5,11.4,11.6,11.8,11.2,10.7,10.3,7.9,4.8,3.3,3.2,3.6],
            winddirection10m: [Int] =  [224,226,240,244,246,251,236,238,245,242,239,230,230,263,252,256,265,260,258,259,260,260,259,261,257,257,271,274,275,277,270,274,271,270,265,257,256,254,251,255,264,257,244,221,147,193,165,146,154,180,207,224,228,216,210,204,208,198,182,184,213,222,228,242,249,292,281,286,276,275,276,276,283,287,304,311,311,303,290,319,318,314,310,301,288,289,294,295,296,297,306,296,285,282,280,278,274,270,264,266,268,273,268,293,314,322,324,326,324,317,309,304,304,306,306,300,278,246,232,225,217,207,197,183,143,113,103,97,97,98,112,153,198,333,344,343,329,312,301,299,295,294,290,284,280,276,276,277,270,262,262,284,305,310,305,301,297,294,292,293,303,320,331,330,312,283,270,276],
            weathercode: [Int] =  [51,61,3,51,61,51,61,3,0,3,3,3,3,3,53,3,51,3,3,3,3,2,3,3,3,2,3,3,3,3,3,2,3,3,0,2,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,2,3,3,3,51,55,55,61,61,51,51,53,51,53,61,63,61,61,51,2,2,3,1,0,0,1,1,1,2,0,0,0,0,0,0,51,51,1,0,0,0,0,0,0,3,2,0,0,0,1,0,0,1,1,1,1,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,45,45,45,1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,3,3,3]
        ) -> Hourly {
            return Hourly(
                time: time,
                temperature2m: temperature2m,
                precipitationProbability: precipitationProbability,
                precipitation: precipitation,
                windspeed10m: windspeed10m,
                winddirection10m: winddirection10m,
                weathercode: weathercode
            )
        }
        
        public static func makeHourlyWeatherModel(
            latitude: Double = 52.52,
            longitude: Double = 13.419998,
            generationtimeMs: Double = 0.7989406585693359,
            utcOffsetSeconds: Int = 7200,
            timezone: String = "Europe/Berlin",
            timezoneAbbreviation: String = "CEST",
            elevation: Double = 38.0,
            hourlyUnits: HourlyUnits? = makeHourlyWeatherUnits(),
            hourly: Hourly? = makeHourlyWeather(),
            dailyUnits: DailyUnits? = nil,
            daily: Daily? = nil
        ) -> WeatherModel {
            return WeatherModel(
                latitude: latitude,
                longitude: longitude,
                generationtimeMs: generationtimeMs,
                utcOffsetSeconds: utcOffsetSeconds,
                timezone: timezone,
                timezoneAbbreviation: timezoneAbbreviation,
                elevation: elevation,
                hourlyUnits: hourlyUnits,
                hourly: hourly,
                dailyUnits: dailyUnits,
                daily: daily
            )
        }
    }
}

