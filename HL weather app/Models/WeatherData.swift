//
//  WeatherData.swift
//  HL weather app
//
//  Created by Kyle Dold on 08/08/2023.
//

// MARK: - WeatherData Structs

struct WeatherData {
    let weatherData: WeatherModel
    let locationData: LocationModel
}

struct HourlyWeatherData {
    let hourlyUnits: WeatherModel.HourlyUnits
    let hourly: WeatherModel.Hourly
}

struct DailyWeatherData {
    let dailyUnits: WeatherModel.DailyUnits
    let daily: WeatherModel.Daily
}

// MARK: - weathercode Dict

let weathercodeIcons: [Int: String] = [
    0: "sun.max",
    1: "cloud.sun",
    2: "cloud.sun",
    3: "cloud.sun",
    45: "cloud.fog",
    48: "cloud.fog",
    51: "cloud.drizzle",
    53: "cloud.drizzle",
    55: "cloud.drizzle",
    56: "cloud.sleet",
    57: "cloud.sleet",
    61: "cloud.rain",
    63: "cloud.rain",
    65: "cloud.rain",
    66: "cloud.sleet",
    67: "cloud.sleet",
    71: "cloud.snow",
    73: "cloud.snow",
    75: "cloud.snow",
    77: "cloud.snow",
    80: "cloud.heavyrain",
    81: "cloud.heavyrain",
    82: "cloud.heavyrain",
    85: "cloud.snow",
    86: "cloud.snow",
    95: "cloud.bolt.rain",
    96: "cloud.bolt.rain",
    99: "cloud.bolt.rain"
]

// MARK: - ForecastSelection

enum ForecastSelection: String, CaseIterable {
    case temp = "Temperature"
    case rain = "Precipitation"
    case wind = "Wind"
}

// MARK: - ScheduleSelection

enum ScheduleSelection: String, CaseIterable {
    case daily = "Daily"
    case hourly = "Hourly"
}
