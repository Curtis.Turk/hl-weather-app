//
//  Location.swift
//  HL weather app
//
//  Created by Curtis Turk on 17/07/2023.
//

import Foundation

struct LocationModel: Codable {
    let results: [Result]
    let generationtimeMS: Double
    
    enum CodingKeys: String, CodingKey {
        case results
        case generationtimeMS = "generationtime_ms"
    }
    
    struct Result: Codable {
        let id: Int
        let name: String
        let latitude, longitude: Double
        let elevation: Int
        let featureCode, countryCode: String
        let admin1ID: Int
        let admin2ID, admin3ID,admin4ID : Int?
        let timezone: String
        let population: Int?
        let countryID: Int?
        let country: String?
        let admin1: String
        let admin2, admin3, admin4: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name, latitude, longitude, elevation
            case featureCode = "feature_code"
            case countryCode = "country_code"
            case admin1ID = "admin1_id"
            case admin2ID = "admin2_id"
            case admin3ID = "admin3_id"
            case admin4ID = "admin4_id"
            case timezone, population
            case countryID = "country_id"
            case country, admin1, admin2, admin3, admin4
        }
        
        
        struct StubFactory {
            public static func makeLocationResult(
                id: Int = 2654675,
                name: String = "Bristol",
                latitude: Double = 51.45523,
                longitude: Double = -2.59665,
                elevation: Int = 21,
                featureCode: String = "PPLA2",
                countryCode: String = "GB",
                admin1ID: Int = 6269131,
                admin2ID: Int? = nil,
                admin3ID: Int? = nil,
                admin4ID: Int? = nil,
                timezone: String = "Europe/London",
                population: Int? = 617280,
                countryID: Int? = 2635167,
                country: String? = "United Kingdom",
                admin1: String = "England",
                admin2: String? = nil,
                admin3: String? = nil,
                admin4: String? = nil
            ) -> Result {
                return Result(
                    id: id,
                    name: name,
                    latitude: latitude,
                    longitude: longitude,
                    elevation: elevation,
                    featureCode: featureCode,
                    countryCode: countryCode,
                    admin1ID: admin1ID,
                    admin2ID: admin2ID,
                    admin3ID: admin3ID,
                    admin4ID: admin4ID,
                    timezone: timezone,
                    population: population,
                    countryID: countryID,
                    country: country,
                    admin1: admin1,
                    admin2: admin2,
                    admin3: admin3,
                    admin4: admin4
                )
            }
        }
    }
}
