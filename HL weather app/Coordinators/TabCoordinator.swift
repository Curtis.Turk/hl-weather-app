//
//  TabCoordinator.swift
//  HL weather app
//
//  Created by Curtis Turk on 19/07/2023.
//

import UIKit
import SwiftUI

enum TabBarPage {
    case current
    case search
    
    func title() -> String {
        switch self {
        case .current:
            return "Current Location"
        case .search:
            return "Search"
        }
    }
    
    func icon() -> UIImage {
        switch self {
        case .current:
            return UIImage(systemName: "sun.max")!
        case .search:
            return UIImage(systemName: "magnifyingglass")!
        }
    }
    
    func tabOrderNumber() -> Int {
        switch self {
        case .current:
            return 0
        case .search:
            return 1
        }
    }
    
    func view() -> some View {
        let scheduleSelectionStore = ScheduleSelectionStore()
        
        switch self {
            case .current:
                return AnyView(
                    CurrentTabView(
                        viewModel: CurrentTabViewModel()
                    ).environmentObject(scheduleSelectionStore)
                )
            case .search:
                return AnyView(
                    SearchTabView(
                        viewModel: SearchTabViewModel()
                    ).environmentObject(scheduleSelectionStore)
                )
        }
    }
}

class TabCoordinator: NSObject, Coordinator {
    
    weak var finishDelegate: CoordinatorFinishDelegate?
    
    var childCoordinators: [Coordinator] = []
    var navController: UINavigationController
    
    var tabBarController: TabBarViewController
    
    required init(_ navigationController: UINavigationController) {
        self.navController = navigationController
        self.tabBarController = TabBarViewController()
    }

    func start() {
        let tabs: [TabBarPage] = [
            .current,
            .search
        ]
        let controllers: [UINavigationController] = tabs.map { getTabController($0) }
        tabBarController.selectedIndex = 0
        prepareTabBarController(withTabControllers: controllers)
    }
    
    func getTabController(_ tab: TabBarPage) -> UINavigationController {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        
        let navController = UINavigationController()
        navController.setNavigationBarHidden(true, animated: false)
        
        navController.tabBarItem = UITabBarItem(
            title: tab.title(),
            image: tab.icon(),
            tag: tab.tabOrderNumber()
        )
        let viewController = UIHostingController(rootView: tab.view())
        navController.pushViewController(viewController, animated: true)

        return navController
    }
    
    private func prepareTabBarController(withTabControllers tabControllers: [UIViewController]) {
        tabBarController.delegate = self
        tabBarController.setViewControllers(tabControllers, animated: true)
        navController.viewControllers = [tabBarController]
    }
}

extension TabCoordinator: UITabBarControllerDelegate {
    func tabBarController(
        _ tabBarController: UITabBarController,
        didSelect viewController: UIViewController
    ) {
    }
}
