//
//  AppDelegate.swift
//  HL weather app
//
//  Created by Curtis Turk on 17/07/2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var tabBarCoordinator: TabCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let navController = UINavigationController()
        
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        tabBarCoordinator = TabCoordinator(navController)
        tabBarCoordinator?.start()
        
        return true
    }
}

