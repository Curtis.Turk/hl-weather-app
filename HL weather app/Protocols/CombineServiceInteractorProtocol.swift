//
//  CombineServiceInteractorProtocol.swift
//  HL weather app
//
//  Created by Curtis Turk on 22/02/2024.
//

import Foundation
import Combine

protocol CombineServiceInteractorProtocol {
    func getLocationPublisher(for locationName: String) -> AnyPublisher<LocationModel, Error>
    
    func getDailyWeatherDataPublisher(latitude: Double, longitude: Double, timezone: String) -> AnyPublisher<WeatherModel, Error>
}
