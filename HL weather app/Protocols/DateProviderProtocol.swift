//
//  DateProvider.swift
//  HL weather app
//
//  Created by Curtis Turk on 15/08/2023.
//

import Foundation

protocol DateProvider {
    var currentDate: Date { get }
}
