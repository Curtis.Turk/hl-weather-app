//
//  BackedViewProvider.swift
//  HL weather app
//
//  Created by Curtis Turk on 18/07/2023.
//

import UIKit

public protocol BackedViewProvider where Self: UIViewController {

    associatedtype View: UIView

    var backedView: View { get }
}

public extension BackedViewProvider {

    var backedView: View {
        guard let view = view as? View else {
            fatalError("Set `view` is not of \(View.self) type.")
        }
        
        return view
    }
}

