//
//  WeatherDataSourceProtocol.swift
//  HL weather app
//
//  Created by Curtis Turk on 15/08/2023.
//

import Foundation

protocol WeatherDataSourceProtocol {
    func fetchUserLocationData(
       schedule: ScheduleSelection,
       completion: @escaping (Result<WeatherData, Error>) -> Void
    ) async -> Void
   
   func fetchUserSearchLocationData(
       for locationToSearch: String,
       schedule: ScheduleSelection,
       completion: @escaping (Result<WeatherData, Error>) -> Void
   ) -> Void
}
