//
//  ScheduleSelectionStore.swift
//  HL weather app
//
//  Created by Curtis Turk on 09/08/2023.
//

import Foundation

final class ScheduleSelectionStore: ObservableObject {
    @Published var scheduleSelection = ScheduleSelection.daily
}
