//
//  ForecastSelectorView.swift
//  HL weather app
//
//  Created by Curtis Turk on 01/08/2023.
//

import SwiftUI



struct ForecastSelectorView: View {
    
    @Binding var forecastSelection: ForecastSelection
    
    var body: some View {
        Picker("",selection: $forecastSelection){
            ForEach(ForecastSelection.allCases, id: \.self){ forecast in
                Text(forecast.rawValue)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
        .background(.white)
        .cornerRadius(30)
        .padding()
    }
}


struct ForecastSelectionView_Previews: PreviewProvider {
    @State static var forecastSelection: ForecastSelection = ForecastSelection.temp
    
    static var previews: some View {
        ForecastSelectorView(forecastSelection: $forecastSelection)
    }
}
