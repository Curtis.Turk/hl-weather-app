//
//  ScheduleSelectorView.swift
//  HL weather app
//
//  Created by Curtis Turk on 03/08/2023.
//

import SwiftUI

struct ScheduleSelectorView: View {
    
    @EnvironmentObject var schedule: ScheduleSelectionStore
    
    var body: some View {
        Picker("", selection: $schedule.scheduleSelection) {
            ForEach(ScheduleSelection.allCases, id: \.self){ schedule in
                Text(schedule.rawValue)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
        .background(.white)
        .cornerRadius(30)
        .padding()
    }
}

struct ScheduleSelectorView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleSelectorView().environmentObject(ScheduleSelectionStore())
    }
}

