//
//  CurrentTabViewModel.swift
//  HL weather app
//
//  Created by Kyle Dold on 01/08/2023.
//

import Foundation
import Combine

class CurrentTabViewModel: ObservableObject {
    
    @Published var locationDetailsViewModel: LocationDetailsViewModel?
    @Published var weatherInformationViewModel: WeatherInformationViewModel?
    
    let backgroundImage = "Clouds"
    let titleText = "Your Local Weather"
    let titleIconString = "location.fill"
    private let dataSource: WeatherDataSourceProtocol
    
    private var cancellables: Set<AnyCancellable> = []
    private let serviceInteractor = ServiceInteractor.shared
    private let combineServiceInteractor: CombineServiceInteractorProtocol
    
    @Published var isUsingCombine = true
    @Published var hourlyWeather: WeatherModel?
    @Published var dailyWeather: WeatherModel?
    @Published var location: LocationModel.Result?
    
    
    var locationErrorMessage: String?
    var weatherErrorMessage: String?
    
    init(
        dataSource: WeatherDataSourceProtocol = WeatherDataSource(),
        combineServiceInteractor: CombineServiceInteractorProtocol =  CombineServiceInteractor.shared,
        locationDetailsViewModel: LocationDetailsViewModel? = nil,
        weatherInformationViewModel: WeatherInformationViewModel? = nil
    ) {
        self.dataSource = dataSource
        self.combineServiceInteractor = combineServiceInteractor
        self.locationDetailsViewModel = locationDetailsViewModel
        self.weatherInformationViewModel = weatherInformationViewModel
    }
    
    func viewAppeared(schedule: ScheduleSelection) async {
        await dataSource.fetchUserLocationData(schedule: schedule){ [weak self] result in
            print("fetching Async Data")
            switch result {
            case .success(let model):
                if let locationResult = model.locationData.results.first {
                    DispatchQueue.main.async { [weak self] in
                        self?.locationDetailsViewModel = LocationDetailsViewModel(
                            locationResult: locationResult
                        )
                    }
                }
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.weatherInformationViewModel = WeatherInformationViewModel(
                        weatherData: model.weatherData
                    )
                }
            case .failure:
                break
            }
        }
    }
    
    func fetchLocationDataUsingPublishers() {
        print("fetching location publisher")
        combineServiceInteractor.getLocationPublisher(for: "Bristol")
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    self.fetchWeatherDataBasedOnLocation()
                case .failure(let error):
                    self.locationErrorMessage = error.localizedDescription
                }
            } receiveValue: { location in
                self.location = location.results.first
            }
            .store(in: &cancellables)
    }
    
    func fetchWeatherDataBasedOnLocation() {
        print("fetching weather publisher")
        if let location = location {
            combineServiceInteractor.getDailyWeatherDataPublisher(
                latitude: location.latitude,
                longitude: location.longitude,
                timezone: location.timezone
            ).sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.weatherErrorMessage = error.localizedDescription
                }
            } receiveValue: { weather in
                self.dailyWeather = weather
            }
            .store(in: &cancellables)
        }
    }
}
