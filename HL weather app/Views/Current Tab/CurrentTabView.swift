//
//  CurrentView.swift
//  HL weather app
//
//  Created by Curtis Turk on 19/07/2023.
//

import SwiftUI

struct CurrentTabView: View {
    
    @ObservedObject var viewModel: CurrentTabViewModel
    @EnvironmentObject var schedule: ScheduleSelectionStore
    
    var body: some View {
        ZStack{
            Image(viewModel.backgroundImage)
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
                .opacity(0.6)
            VStack {
                HStack{
                    Text(viewModel.titleText)
                        .font(.largeTitle)
                    Image(systemName: viewModel.titleIconString)
                        .font(.title)
                }
                
                if viewModel.isUsingCombine {
                    if let locationResult = viewModel.location {
                        LocationDetailsView(viewModel: LocationDetailsViewModel(locationResult: locationResult))
                    } else {
                        loadingView(space: 50)
                    }
                    
                    if let dailyWeather = viewModel.dailyWeather {
                        WeatherInformationView(viewModel: WeatherInformationViewModel(weatherData: dailyWeather))
                    } else {
                        ScheduleSelectorView(schedule: _schedule)
                        loadingView(space: 100)
                        ScheduleSelectorView(schedule: _schedule)
                    }
                } else {
                    if let locationDetailsViewModel = viewModel.locationDetailsViewModel {
                        LocationDetailsView(
                            viewModel: locationDetailsViewModel
                        )
                    } else {
                        loadingView(space: 50)
                    }
                    if let weatherInformationViewModel = viewModel.weatherInformationViewModel {
                        WeatherInformationView(
                            viewModel: weatherInformationViewModel
                        )
                    } else {
                        ScheduleSelectorView(schedule: _schedule)
                        loadingView(space: 100)
                        ScheduleSelectorView(schedule: _schedule)
                    }
                    
                }
                
                Text("Networking method: \(viewModel.isUsingCombine ? "Combine" : "Async Await")")
            }
            .frame(width: 360, height: 625)
            .onReceive(schedule.$scheduleSelection) { newSchedule in
                if viewModel.isUsingCombine {
                    viewModel.fetchLocationDataUsingPublishers()
                } else {
                    Task{
                        await viewModel.viewAppeared(schedule: newSchedule)
                    }
                }
            }
        }
    }
    
    func loadingView(space: CGFloat) -> some View {
        HStack {
            Spacer()
                .padding(space)
        }
        .padding()
        .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 30))
    }
}

struct CurrentView_Previews: PreviewProvider {
    static var previews: some View {
        CurrentTabView(
            viewModel: CurrentTabViewModel(
                locationDetailsViewModel: LocationDetailsViewModel(
                    locationResult: LocationModel.Result.StubFactory.makeLocationResult()
                ),
                weatherInformationViewModel: WeatherInformationViewModel(
                    weatherData: WeatherModel.StubFactory.makeDailyWeatherModel()
                )
            )
        ).environmentObject(ScheduleSelectionStore())
    }
}
