//
//  DailyWeatherView.swift
//  HL weather app
//
//  Created by Curtis Turk on 03/08/2023.
//

import SwiftUI

struct DailyWeatherView: View {
    
     let viewModel: DailyWeatherViewModel
    
    var body: some View {
        ForEach(viewModel.dailyWeather.time.indices, id: \.self){ index in
            GeometryReader { geometry in
                VStack{
                    Text(getWeekday(for: viewModel.dailyWeather.time[index]))
                    Image(systemName:
                            (weathercodeIcons[viewModel.dailyWeather.weathercode[index]] ?? viewModel.defaultWeatherIcon))
                        .font(.system(size: 32))
                        .frame(width: 40, height: 40)
                        .padding(.bottom)
                    switch viewModel.forecastSelection {
                        case .temp:
                            dailyTempView(index: index)
                        case .rain:
                            dailyRainView(index: index)
                        case .wind:
                            dailyWindView(index: index)
                    }
                }.frame(width: geometry.size.width)
            }
        }
    }
}

extension DailyWeatherView {
    
    private func dailyTempView(index: Int) -> some View {
        let maxTemp = String(format: viewModel.oneSF, viewModel.dailyWeather.temperature2mMax[index])
        let minTemp = String(format: viewModel.oneSF, viewModel.dailyWeather.temperature2mMin[index])
        
        return VStack {
            unitDisplayIfToday(unit: viewModel.highTempText, index: index)
                .font(.caption)
                .foregroundColor(.red)
            Text(maxTemp)
                .font(.title3)
                .padding(.bottom)

            unitDisplayIfToday(unit: viewModel.lowTempText, index: index)
                .font(.caption)
                .foregroundColor(.blue)
            Text(minTemp)
                .font(.title3)
        }
    }
    
    private func dailyRainView(index: Int) -> some View {
        let rain = viewModel.dailyWeather.precipitationSum[index].description
        
        return VStack{
            unitDisplayIfToday(unit: viewModel.rainText, index: index)
                .font(.caption)
            Text(rain)
                .font(.title3)
        }
    }
    
    private func dailyWindView(index: Int) -> some View {
        let windSpeed = String(format: viewModel.oneSF, viewModel.dailyWeather.windspeed10mMax[index])
        let windDir = viewModel.dailyWeather.winddirection10mDominant[index]

        return VStack{
            unitDisplayIfToday(unit: viewModel.windSpeedText, index: index)
                .font(.caption)
            Text(windSpeed)
                .font(.title3)
                .padding(.bottom)

            unitDisplayIfToday(unit: viewModel.windDirText, index: index)
                .font(.system(size: 9))
            Image(systemName: viewModel.arrowText)
                .rotationEffect(.degrees(Double(windDir)))
                .frame(width: 20, height: 20)
        }
    }
    
    private func unitDisplayIfToday(unit: String, index: Int) -> some View {
        return viewModel.isToday(index) ?
            Text(unit)
            : Text(viewModel.spacerText)
    }
}

struct DailyWeatherView_Previews: PreviewProvider {
    static var previews: some View {
        HStack{
            DailyWeatherView(
                viewModel: DailyWeatherViewModel(
                    dailyWeather: WeatherModel.StubFactory.makeDailyWeather(),
                    dailyUnits: WeatherModel.StubFactory.makeDailyWeatherUnits(),
                    forecastSelection: ForecastSelection.temp
                )
            )
        }
        HStack {
            DailyWeatherView(
                viewModel: DailyWeatherViewModel(
                    dailyWeather: WeatherModel.StubFactory.makeDailyWeather(),
                    dailyUnits: WeatherModel.StubFactory.makeDailyWeatherUnits(),
                    forecastSelection: ForecastSelection.wind
                )
            )
        }
    }
}
