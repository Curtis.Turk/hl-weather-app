//
//  DailyWeatherViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 04/08/2023.
//

import Foundation

class DailyWeatherViewModel: ObservableObject {
    
    let dailyWeather: WeatherModel.Daily
    let dailyUnits: WeatherModel.DailyUnits
    let forecastSelection: ForecastSelection

    let defaultWeatherIcon = "sun.max"
    let oneSF = "%.1f"
    let highTempText = "Hi °C"
    let lowTempText = "Lo °C"
    let rainText = "mm"
    let windSpeedText = "km/h"
    let windDirText = "Direction"
    let spacerText = " "
    let arrowText = "arrow.down"
    
    init(
        dailyWeather: WeatherModel.Daily,
        dailyUnits: WeatherModel.DailyUnits,
        forecastSelection: ForecastSelection
    ) {
        self.dailyWeather = dailyWeather
        self.dailyUnits = dailyUnits
        self.forecastSelection = forecastSelection
    }
    
    func isToday(_ index: Int) -> Bool { index == 0 }
}
