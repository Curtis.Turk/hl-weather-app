//
//  ForecastViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 03/08/2023.
//

import Foundation

class ForecastViewModel: ObservableObject {
    
    let weatherData: WeatherModel
    var forecastSelection: ForecastSelection
    
    init(
        weatherData: WeatherModel,
        forecastSelection: ForecastSelection
    ) {
        self.weatherData = weatherData
        self.forecastSelection = forecastSelection
    }
}

