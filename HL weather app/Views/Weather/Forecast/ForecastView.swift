//
//  ForecastView.swift
//  HL weather app
//
//  Created by Curtis Turk on 01/08/2023.
//

import SwiftUI

struct ForecastView: View {
    
    @ObservedObject var viewModel: ForecastViewModel

    var body: some View {
        HStack{
            if let dailyWeather = viewModel.weatherData.daily {
                DailyWeatherView(
                    viewModel: DailyWeatherViewModel(
                        dailyWeather: dailyWeather,
                        dailyUnits: viewModel.weatherData.dailyUnits!,
                        forecastSelection: viewModel.forecastSelection
                    )
                )
            } else if let hourlyWeather = viewModel.weatherData.hourly {
                HourlyWeatherView(
                    viewModel: HourlyWeatherViewModel(
                        hourlyWeather: hourlyWeather,
                        hourlyUnits: viewModel.weatherData.hourlyUnits!,
                        forecastSelection: viewModel.forecastSelection
                    )
                )
            } else {
                Text("No data")
            }
        }
    }
}

struct ForecastView_Previews: PreviewProvider {
    
    @State static var forecastSelection : ForecastSelection = ForecastSelection.temp
    
    static var previews: some View {
        ForecastView(
            viewModel: ForecastViewModel(
                weatherData: WeatherModel.StubFactory.makeDailyWeatherModel(),
                forecastSelection: ForecastSelection.temp
            )
        ).environmentObject(ScheduleSelectionStore())
    }
}
