//
//  HourlyWeatherViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 09/08/2023.
//

import Foundation

class HourlyWeatherViewModel: ObservableObject {
    
    let hourlyWeather: WeatherModel.Hourly
    let hourlyUnits: WeatherModel.HourlyUnits
    let forecastSelection: ForecastSelection
    
    let defaultWeatherIcon = "sun.max"
    let oneSF = "%.1f"
    let tempText = "°C"
    let rainText = "mm"
    let rainProbabilityText = "%"
    let rainProbDefaultText = "0"
    let windSpeedText = "km/h"
    let windDirText = "Direction"
    let arrowIconText = "arrow.down"
    let nowText = "Now"
    let tomorrowText = "Tomorrow"
    let spacerText = " "
    
    var hours: [String] { extractCurrentHours(
            from: hourlyWeather.time,
            numberOfDays: 1,
            dateProvider: CurrentDateProvider()
        )
    }
    var weatherCodeIconTexts: [String] {
        buildWeatherCodeIconTextsArray(for: hourlyWeather.weathercode)
    }
    var temperature: [Double] { hourlyWeather.temperature2m }
    var precipitation: [Double] { hourlyWeather.precipitation }
    var precipitationProbability: [Int?] { hourlyWeather.precipitationProbability }
        
    init(
        hourlyWeather: WeatherModel.Hourly,
        hourlyUnits: WeatherModel.HourlyUnits,
        forecastSelection: ForecastSelection
    ) {
        self.hourlyWeather = hourlyWeather
        self.hourlyUnits = hourlyUnits
        self.forecastSelection = forecastSelection
    }
    
    func isTodayOrEverySixDays(_ index: Int) -> Bool { (isToday(index) || index % 5 == 0) }
    func isToday(_ index: Int) -> Bool { index == 0 }
    func isTomorrow(_ hour: String) -> Bool { hour == "00" }
    
    private func buildWeatherCodeIconTextsArray(for weathercodes: [Int]) -> [String] {
        return weathercodes.map { weathercodeIcons[$0] ?? self.defaultWeatherIcon }
    }
}
