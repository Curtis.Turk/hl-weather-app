//
//  HourlyWeatherView.swift
//  HL weather app
//
//  Created by Curtis Turk on 31/07/2023.
//

import SwiftUI

struct HourlyWeatherView: View {
    
    let viewModel: HourlyWeatherViewModel
    
    var body: some View {
        ScrollView(.horizontal){
            HStack{
                ForEach(viewModel.hours.indices, id: \.self){ index in
                    VStack{
                        TodayOrTomorrowTextView(index: index, hour: viewModel.hours[index])
                        Text(viewModel.hours[index])
                        Image(systemName: viewModel.weatherCodeIconTexts[index])
                            .font(.system(size: 45))
                            .frame(width: 60, height: 60)
                       
                        switch viewModel.forecastSelection {
                        case .temp:
                            hourlyTempView(index: index)
                        case .rain:
                            hourlyRainView(index: index)
                        case .wind:
                            hourlyWindView(index: index)
                        }
                        Spacer()
                    }
                }
            }
        }
    }
}

extension HourlyWeatherView {
    
    private func hourlyTempView(index: Int) -> some View {
        let temp = String(format: viewModel.oneSF, viewModel.temperature[index])

        return VStack{
            WeatherUnitsView(index: index, unit: viewModel.tempText)
            Text(temp)
                .font(.title3)
        }
    }
    private func hourlyRainView(index: Int) -> some View {
        let rain = String(format: viewModel.oneSF, viewModel.precipitation[index])
        let rainProbability = viewModel.precipitationProbability[index]?.description
        
        return VStack{
            WeatherUnitsView(index: index, unit: viewModel.rainText)
            Text(rain)
                .font(.title3)
                .padding(.bottom, 1)
            
            WeatherUnitsView(index: index, unit: viewModel.rainProbabilityText)
            Text(rainProbability ?? viewModel.rainProbDefaultText)
                .font(.title3)
        }
    }
    private func hourlyWindView(index: Int) -> some View {
        let windSpeed = String(format: viewModel.oneSF, viewModel.hourlyWeather.windspeed10m[index])
        let windDir = viewModel.hourlyWeather.winddirection10m[index]

        return VStack{
            WeatherUnitsView(index: index, unit: viewModel.windSpeedText)
            Text(windSpeed)
                .font(.title3)
                .padding(.bottom, 1)
            
            WeatherUnitsView(index: index, unit: viewModel.windDirText)
            Image(systemName: viewModel.arrowIconText)
                .rotationEffect(.degrees(Double(windDir)))
                .padding(.top, 2)
        }
    }
    private func WeatherUnitsView(index: Int, unit: String) -> some View {
        let unitText = viewModel.isTodayOrEverySixDays(index) ? unit : viewModel.spacerText
            
            return Text(unitText)
                .font(.caption2)
                .padding(.bottom, 1)
    }
    private func TodayOrTomorrowTextView(index: Int, hour: String) -> some View{
        var dayText = viewModel.spacerText
        
        if viewModel.isToday(index) {
            dayText = viewModel.nowText
        } else if viewModel.isTomorrow(hour) {
            dayText = viewModel.tomorrowText
        }
        return Text(dayText)
                .font(.caption)
                .padding(.bottom, 1)
    }
}



struct DisplayHourlyWeatherView_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            HourlyWeatherView(
                viewModel: HourlyWeatherViewModel(
                    hourlyWeather: WeatherModel.StubFactory.makeHourlyWeather(),
                    hourlyUnits: WeatherModel.StubFactory.makeHourlyWeatherUnits(),
                    forecastSelection: ForecastSelection.temp))
            .environmentObject(ScheduleSelectionStore())
        }
    }
}
