//
//  WeatherInformationViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 02/08/2023.
//

import Foundation

class WeatherInformationViewModel: ObservableObject {
    
    @Published var forecastSelection: ForecastSelection = ForecastSelection.temp

    let weatherData: WeatherModel
    
    init(weatherData: WeatherModel) {
        self.weatherData = weatherData
    }
}
