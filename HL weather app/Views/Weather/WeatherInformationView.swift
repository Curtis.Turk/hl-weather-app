//
//  WeatherInformationView.swift
//  HL weather app
//
//  Created by Curtis Turk on 27/07/2023.
//

import SwiftUI

struct WeatherInformationView: View {
    
    @ObservedObject var viewModel: WeatherInformationViewModel
    @EnvironmentObject var schedule: ScheduleSelectionStore
    
    var body: some View {
        VStack{
            ScheduleSelectorView(
            ).environmentObject(schedule)
            ForecastView(
                viewModel: ForecastViewModel(
                    weatherData: viewModel.weatherData,
                    forecastSelection: viewModel.forecastSelection
                )
            )
            .padding()
            .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 30))
            ForecastSelectorView(
                forecastSelection: $viewModel.forecastSelection
            )
        }
    }
}

struct WeatherInformationView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherInformationView(
            viewModel: WeatherInformationViewModel(weatherData: WeatherModel.StubFactory.makeDailyWeatherModel())
        ).environmentObject(ScheduleSelectionStore())
    }
}
