//
//  CurrentView.swift
//  HL weather app
//
//  Created by Curtis Turk on 19/07/2023.
//

import SwiftUI

struct CurrentTabView: View {
    
    @StateObject var locationManagerModel = LocationManagerModel()
    
    @State private var locationData: LocationModel?
    @State private var weatherData: WeatherModel?
    
    
    var body: some View {
        ZStack{
            VStack{
                LocationTitleView(locationData: $locationData).padding([.bottom], 20)
                LocationDetailsView(locationData: $locationData).padding([.bottom], 20)
                WeatherInformationView(weatherData: $weatherData)
            }.frame(width: 300, height: 625)
        }.onAppear {
            locationManagerModel.checkIfLocationServicesIsEnabled()
            fetchData()
        }
    }
    
    private func fetchData() {
        Task {
            do {
                await locationManagerModel.getLocationString()
                
                let locationDataResponse = try await LocationData().getLocationData(for: locationManagerModel.userLocation.city ?? "Bristol")
                self.locationData = locationDataResponse
                
                if let locationData = locationData {
                    let weatherDataResponse = try await WeatherData().getDailyWeatherData(latitude: locationData.results[0].latitude, longitude: locationData.results[0].longitude, timezone: locationData.results[0].timezone)
                    self.weatherData = weatherDataResponse
                }
            } catch {
                print("Error: \(error)")
            }
        }
    }
    
    private func setHourlyWeatherData() {
        Task {
            do{
                let weatherDataResponse = try await WeatherData().getHourlyWeatherData(latitude: locationManagerModel.userLocation.latitude, longitude: locationManagerModel.userLocation.longitude)
                self.weatherData = weatherDataResponse
            }catch{
                print("Error: \(error)")
            }
        }
    }
}

struct CurrentView_Previews: PreviewProvider {
    static var previews: some View {
        CurrentTabView()
    }
}
