//
//  LocationDetailsViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 02/08/2023.
//

import Foundation

class LocationDetailsViewModel: ObservableObject {
    
    var latText = "Latitude:"
    var longText = "Longitude:"
    
    var name: String { location.name }
    var countryCode: String { location.countryCode}
    var admin: String { location.admin1 }
    var timezone: String { location.timezone }
    var latitude: String { location.latitude.description }
    var longitude: String { location.longitude.description }
    
    let location: LocationModel.Result
    
    init(locationResult: LocationModel.Result) {
        self.location = locationResult
    }
}
