//
//  LocationDetailsView.swift
//  HL weather app
//
//  Created by Curtis Turk on 27/07/2023.
//

import SwiftUI

struct LocationDetailsView: View {
    
    @ObservedObject var viewModel: LocationDetailsViewModel
    
    var body: some View {
        VStack{
            HStack{
                Text(viewModel.name)
                    .bold()
                    .font(.title)
                Spacer()
                Text(viewModel.countryCode)
            }
            HStack{
                Text(viewModel.admin)
                Spacer()
                Text(viewModel.timezone)
            }.padding([.bottom], 20)
            
            HStack{
                Text(viewModel.latText)
                Spacer()
                Text(viewModel.latitude)
            }
            HStack{
                Text(viewModel.longText)
                Spacer()
                Text(viewModel.longitude)
            }
        }.padding().background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 30))
    }
}


struct Previews_LocationDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        LocationDetailsView(
            viewModel: LocationDetailsViewModel(
                locationResult: LocationModel.Result.StubFactory.makeLocationResult()
            )
        )
    }
}
