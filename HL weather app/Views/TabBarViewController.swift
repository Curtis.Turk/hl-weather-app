//
//  ViewController.swift
//  HL weather app
//
//  Created by Curtis Turk on 17/07/2023.
//

import UIKit
import SwiftUI

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.alpha = 0.6
        tabBar.tintColor = .blue
        tabBar.backgroundColor = .white
    }
}
