//
//  SearchView.swift
//  HL weather app
//
//  Created by Curtis Turk on 19/07/2023.
//

import SwiftUI

struct SearchTabView: View {
    
    @ObservedObject var viewModel: SearchTabViewModel
    @EnvironmentObject var schedule: ScheduleSelectionStore
    
    var body: some View {
        ZStack{
            Image(viewModel.backgroundImage)
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
                .opacity(0.3)
            
            VStack{
                
                searchTextInputView()
                
                if $viewModel.locationToSearch.wrappedValue.isEmpty {
                    displaySuggestedLocations()
                } else {
                    
                    if let locationDetailsViewModel = viewModel.locationDetailsViewModel {
                        LocationDetailsView(
                            viewModel: locationDetailsViewModel
                        )
                    }
                    
                    if let weatherInformationViewModel = viewModel.weatherInformationViewModel {
                        WeatherInformationView(
                            viewModel: weatherInformationViewModel
                        )
                    }
                }
                Spacer()
            }
            .frame(width: 360, height: 625)
            .onReceive(schedule.$scheduleSelection) { newSchedule in
                if !$viewModel.locationToSearch.wrappedValue.isEmpty {
                    viewModel.fetchSearchData(schedule: newSchedule)
                }
            }
        }
    }
    private func searchTextInputView() -> some View {
        TextField(
            viewModel.whereAreYouText,
            text: $viewModel.userSearchLocation
        )
        .foregroundStyle(.blue)
        .padding()
        .background(.white)
        .cornerRadius(30)
        .padding(.bottom)
        .onReceive(viewModel.$userSearchLocation) { _ in
            if !$viewModel.locationToSearch.wrappedValue.isEmpty {
                viewModel.fetchSearchData(
                    schedule: schedule.scheduleSelection
                )
            }
        }
    }
    
    private func displaySuggestedLocations() -> some View {
        VStack{
            Spacer()
            
            Text(viewModel.locationsText)
                .padding()
                .font(.system(size: 18))
                .background(.ultraThinMaterial)
                .cornerRadius(30)
                .padding(.bottom)
            
            ForEach(viewModel.defaultLocations, id: \.self) { location in
                Button(location) {
                    viewModel.userSearchLocation = location
                }
                .padding()
                .background(.white)
                .cornerRadius(30)
                .padding(.bottom)
            }
            
            Spacer()
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchTabView(
            viewModel: SearchTabViewModel()
        ).environmentObject(ScheduleSelectionStore())
    }
}
