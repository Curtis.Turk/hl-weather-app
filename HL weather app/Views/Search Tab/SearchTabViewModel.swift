//
//  SearchTabViewModel.swift
//  HL weather app
//
//  Created by Kyle Dold on 01/08/2023.
//

import Foundation
import Combine

class SearchTabViewModel: ObservableObject {

    @Published var locationDetailsViewModel: LocationDetailsViewModel?
    @Published var weatherInformationViewModel: WeatherInformationViewModel?
    @Published var userSearchLocation: String = ""
    @Published var locationToSearch: String = ""
    
    let backgroundImage = "Umbrellas"
    let whereAreYouText = "Search for a city?"
    let goText = "Go"
    let locationsText = "... or choose one of the options below."
    let defaultLocations = [
        "Bristol",
        "London",
        "New York",
        "Delhi",
        "Tokyo"
    ]
    
    private let dataSource: WeatherDataSource
    private var cancellables = Set<AnyCancellable>()
    
    init(
        dataSource: WeatherDataSource = WeatherDataSource(),
        locationDetailsViewModel: LocationDetailsViewModel? = nil,
        weatherInformationViewModel: WeatherInformationViewModel? = nil
    ) {
        self.dataSource = dataSource
        self.locationDetailsViewModel = locationDetailsViewModel
        self.weatherInformationViewModel = weatherInformationViewModel
        self.debounceTextChanges()
    }
    
    func fetchSearchData(schedule: ScheduleSelection = ScheduleSelection.daily) {
        dataSource.fetchUserSearchLocationData(for: locationToSearch, schedule: schedule){ [weak self] result in
            
            switch result {
            case .success(let model):
                if let locationResult = model.locationData.results.first {
                    DispatchQueue.main.async { [weak self] in
                        self?.locationDetailsViewModel = LocationDetailsViewModel(
                            locationResult: locationResult
                        )
                    }
                }
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.weatherInformationViewModel = WeatherInformationViewModel(
                        weatherData: model.weatherData
                    )
                }
            case .failure:
                break
            }
        }
    }
    
    private func debounceTextChanges() {
        $userSearchLocation
            .dropFirst()
            .debounce(for: 1, scheduler: RunLoop.main)
            .sink {
                self.locationToSearch = $0
                self.fetchSearchData()
            }
            .store (in: &cancellables)
    }
}
