//
//  LocationManager.swift
//  HL weather app
//
//  Created by Curtis Turk on 31/07/2023.
//

import CoreLocation

struct UserLocationData {
    let latitude: Double
    let longitude: Double
    let city: String?
}

class LocationManagerModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager
    var currentLocation: CLLocation?
    
    @Published var userLocation = UserLocationData(latitude: 51.45523, longitude: -2.59665, city: nil)
    
    override init() {
        locationManager = CLLocationManager()
        super.init()
        
        locationManager.delegate = self
    }
    
    func isLocationServicesEnabled() async -> Bool {
        CLLocationManager.locationServicesEnabled()
    }
    
    private func checkLocationAuthorization() {
        switch locationManager.authorizationStatus{
            case .notDetermined: locationManager.requestWhenInUseAuthorization()
            case .restricted: print("Location Services restricted")
            case .denied: print("Location Services denied, go to setting to enable")
            case .authorizedAlways,
                 .authorizedWhenInUse:
                guard let location = locationManager.location else { break }

                userLocation = UserLocationData(
                    latitude: location.coordinate.latitude.magnitude,
                    longitude: location.coordinate.longitude.magnitude,
                    city: nil
                )
            
            @unknown default:
                break
        }
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func getLocationString(completion: @escaping (UserLocationData?) -> Void) {
        guard let location = locationManager.location else { return }
        
        location.fetchCity { [weak self] city, error in
            guard let city = city, error == nil else { return }
            self?.userLocation = UserLocationData(
                latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude,
                city: city
            )
            completion(self?.userLocation)
        }
    }
}


extension CLLocation {
    func fetchCity(completion: @escaping (_ city: String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) {
            completion($0?.first?.locality, $1)
        }
    }
}
