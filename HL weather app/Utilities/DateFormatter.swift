//
//  DateFormatter.swift
//  HL weather app
//
//  Created by Curtis Turk on 31/07/2023.
//

import Foundation

// MARK: Date formatting

func dailyDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
}

func hourlyDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
    return dateFormatter
}

func getWeekday(for date: String) -> String {
    let dateFormatter = dailyDateFormatter()
    
    var weekday = ""
    if let dateObject = dateFormatter.date(from: date) {
        weekday = dateFormatter.shortWeekdaySymbols[Calendar.current.component(.weekday, from: dateObject) - 1]
    }
    return weekday
}

func getHour(for date: String) -> String {
    let dateFormatter = hourlyDateFormatter()

    var hour = ""
    if let date = dateFormatter.date(from: date) {
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "HH"
        hour = hourFormatter.string(from: date)
    }
    return hour
}

// MARK: Date providers

class MockDateProvider: DateProvider {
    var currentDate: Date

    init(currentDate: Date) {
        self.currentDate = currentDate
    }
}

class CurrentDateProvider: DateProvider {
    var currentDate: Date = Date()
}

// MARK: Getting relevant hours

func extractCurrentHours(from hours: [String], numberOfDays: Int, dateProvider: DateProvider) -> [String] {
    let dateFormatter = hourlyDateFormatter()
    let currentDate = dateProvider.currentDate
    let calendar = Calendar.current

    var currentHours = [String]()
    for hour in hours {
        if let date = dateFormatter.date(from: hour) {
            let components = calendar.dateComponents([.day, .hour], from: currentDate, to: date)
            if isHourWithinNumberOfDays(components: components, numberOfDays: numberOfDays) {
               let formattedHour = getHour(for: hour)
               currentHours.append(formattedHour)
            }
        }
    }
    return currentHours
}

func isHourWithinNumberOfDays (components: DateComponents, numberOfDays: Int) -> Bool {
    // Check if the difference is within the next two days and current hour or later
    if  let daysDiff = components.day,
        let hoursDiff = components.hour,
        
        daysDiff >= 0,
        daysDiff < numberOfDays,
        (daysDiff > 0 || hoursDiff >= 0)
    {
        return true
    }
        return false
}


