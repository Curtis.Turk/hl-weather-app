//
//  URLSession+Extensions.swift
//  HL weather app
//
//  Created by Curtis Turk on 21/02/2024.
//

import Foundation

extension URLSession {
    func fetchData<T: Decodable>(from url: URL) async throws -> T {
        do {
            let (data, response) = try await self.data(from: url)
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200..<300).contains(httpResponse.statusCode) else {
                throw NetworkError.invalidResponse
            }
            
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: data)
        } catch let DecodingError.dataCorrupted(context) {
            throw NetworkError.dataCorrupted(context)
        } catch let DecodingError.keyNotFound(key, _) {
            throw NetworkError.keyNotFound(key.stringValue)
        } catch let DecodingError.valueNotFound(value, _) {
            throw NetworkError.valueNotFound(value)
        } catch let DecodingError.typeMismatch(type, _) {
            throw NetworkError.typeMismatch(type)
        } catch {
            throw NetworkError.other(error)
        }
    }
}

enum NetworkError: Error {
    case invalidURL
    case invalidResponse
    case dataCorrupted(Any)
    case keyNotFound(String)
    case valueNotFound(Any)
    case typeMismatch(Any)
    case other(Error)
}
