//
//  WeatherDataSource.swift
//  HL weather app
//
//  Created by Kyle Dold on 08/08/2023.
//

import Foundation
import Combine

class WeatherDataSource: WeatherDataSourceProtocol {
    
    // MARK: - Properties
    
    internal let locationManagerModel = LocationManagerModel()
    private let serviceInteractor: ServiceInteractor
    
    // MARK: - Init
    
    init(serviceInteractor: ServiceInteractor = .shared) {
        self.serviceInteractor = serviceInteractor
    }
    
    // MARK: - Networking
    
    func fetchUserLocationData(
        schedule: ScheduleSelection,
        completion: @escaping (Result<WeatherData, Error>) -> Void
    ) async {
        
        guard await locationManagerModel.isLocationServicesEnabled() else {
            completion(.failure(DataSourceError.locationServicesDisabled))
            return
        }
        
        locationManagerModel.getLocationString { [weak self] location in
            guard let city = location?.city else {
                completion(.failure(DataSourceError.locationNotFound))
                return
            }
            guard let self else {
                completion(.failure(DataSourceError.selfIsNil))
                return
            }
            
            fetchData(for: city, schedule: schedule) { result in
                completion(result)
            }
        }
    }
    
    func fetchUserSearchLocationData(
        for locationToSearch: String,
        schedule: ScheduleSelection = ScheduleSelection.daily,
        completion: @escaping (Result<WeatherData, Error>) -> Void
    ) {
        fetchData(for: locationToSearch, schedule: schedule) { result in
            completion(result)
        }
    }
    
    private func fetchData(
        for location: String,
        schedule: ScheduleSelection = ScheduleSelection.daily,
        completion: @escaping (Result<WeatherData, Error>) -> Void
    ) {
        Task {
            do {
                let locationDataResponse = try await serviceInteractor.getLocationData(for: location)
                guard let locationResult = locationDataResponse.results.first else {
                    completion(.failure(DataSourceError.locationDataNotAvailable))
                    return
                }
                
                let weatherDataResponse = try await setWeatherdataResponse(
                    schedule: schedule,
                    locationResult: locationResult
                )
                
                let model = WeatherData(
                    weatherData: weatherDataResponse,
                    locationData: locationDataResponse
                )
                completion(.success(model))
            } catch {
                print("Error: \(error)")
                completion(.failure(error))
            }
        }
    }
    
    private func setWeatherdataResponse(
        schedule: ScheduleSelection,
        locationResult: LocationModel.Result
    ) async throws -> WeatherModel {
        switch schedule {
        case .daily:
            return try await serviceInteractor.getDailyWeatherData(
                latitude: locationResult.latitude,
                longitude: locationResult.longitude,
                timezone: locationResult.timezone
            )
        case .hourly:
            return try await serviceInteractor.getHourlyWeatherData(
                latitude: locationResult.latitude,
                longitude: locationResult.longitude,
                timezone: locationResult.timezone
            )
        }
    }
}

enum DataSourceError: Error{
    case locationServicesDisabled
    case locationDataNotAvailable
    case selfIsNil
    case locationNotFound
}
