//
//  ServiceInteractor.swift
//  HL weather app
//
//  Created by Kyle Dold on 01/08/2023.
//

import Foundation
import Combine

class ServiceInteractor {
    
    static let shared = ServiceInteractor()
    
    // MARK: - Location
    
    func getLocationData(for locationName: String) async throws -> LocationModel {
        
        guard !locationName.isEmpty else { throw NetworkError.keyNotFound("Empty location") }
        
        let formattedLocation = locationName.replacingOccurrences(of: " " , with: "+")
        
        let fullEndpoint = Endpoints.getLocationEndpoint(location: formattedLocation)

        guard let url = URL(string: fullEndpoint) else {
            throw NetworkError.invalidURL
        }
        
        return try await URLSession.shared.fetchData(from: url)
    }
    
    // MARK: - Weather
    
    func getHourlyWeatherData(latitude: Double, longitude: Double, timezone: String) async throws -> WeatherModel {
        try await getWeatherData(
            with: Endpoints.getHourlyWeatherEndpoint(latitude: latitude, longitude: longitude, timezone: timezone)
        )
    }
    
    func getDailyWeatherData(latitude: Double, longitude: Double, timezone: String) async throws -> WeatherModel {
        try await getWeatherData(
            with: Endpoints.getHourlyWeatherEndpoint(latitude: latitude, longitude: longitude, timezone: timezone)
        )
    }
    
    func getWeatherData(with endpoint: String) async throws -> WeatherModel {
        
        guard let url = URL(string: endpoint) else {
            throw NetworkError.invalidURL
        }
   
        return try await URLSession.shared.fetchData(from: url)
    }
}
