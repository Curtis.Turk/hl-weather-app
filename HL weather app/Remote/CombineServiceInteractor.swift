//
//  CombineServiceInteractor.swift
//  HL weather app
//
//  Created by Curtis Turk on 21/02/2024.
//

import Foundation
import Combine

class CombineServiceInteractor: CombineServiceInteractorProtocol {
    
    static let shared = CombineServiceInteractor()
    
    // MARK: - Location
    
    func getLocationPublisher(for locationName: String) -> AnyPublisher<LocationModel, Error> {
        
        let formattedLocation = locationName.replacingOccurrences(of: " " , with: "+")
        let fullEndpoint = Endpoints.getLocationEndpoint(location: formattedLocation)
        
        guard let url = URL(string: fullEndpoint) else {
            return Fail(error: NetworkError.invalidURL).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse, (200..<300).contains(httpResponse.statusCode) else {
                    throw NetworkError.invalidResponse
                }
                return data
            }
            .decode(type: LocationModel.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    // MARK: - Weather
    
    func getDailyWeatherDataPublisher(latitude: Double, longitude: Double, timezone: String) -> AnyPublisher<WeatherModel, Error> {
        
        let fullEndpoint = Endpoints.getDailyWeatherEndpoint(latitude: latitude, longitude: longitude, timezone: timezone)
        
        guard let url = URL(string: fullEndpoint) else {
            return Fail(error: NetworkError.invalidURL).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse, (200..<300).contains(httpResponse.statusCode) else {
                    throw NetworkError.invalidResponse
                }
                return data
            }
            .decode(type: WeatherModel.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
