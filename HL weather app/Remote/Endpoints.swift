//
//  Endpoints.swift
//  HL weather app
//
//  Created by Curtis Turk on 21/02/2024.
//

import Foundation

/*
 Example API endpoints
 
 Hourly
 https://api.open-meteo.com/v1/forecast?latitude=51.4552&longitude=-2.5966&hourly=temperature_2m,precipitation_probability,precipitation,windspeed_10m,winddirection_10m,weathercode&timezone=Europe/Berlin
 
 Daily
 https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&daily=temperature_2m_max,temperature_2m_min,precipitation_sum,windspeed_10m_max,winddirection_10m_dominant&timezone=Europe/Berlin&daily=weathercode
*/


struct Endpoints {
    static let weatherPrefix = "https://api.open-meteo.com/v1/forecast?"
    static let hourlySuffix = "&hourly=temperature_2m,precipitation_probability,precipitation,windspeed_10m,winddirection_10m,weathercode"
    static let dailySuffix = "&daily=temperature_2m_max,temperature_2m_min,precipitation_sum,windspeed_10m_max,winddirection_10m_dominant&daily=weathercode"
    
    static let locationPrefix = "https://geocoding-api.open-meteo.com/v1/search?name="
    static let locationSuffix = "&count=1&language=en&format=json"
    
    
    static func getHourlyWeatherEndpoint(latitude: Double, longitude: Double, timezone: String) -> String {
        "\(weatherPrefix)latitude=\(latitude)&longitude=\(longitude)\(hourlySuffix)&timezone=\(timezone)"
    }
    static func getDailyWeatherEndpoint(latitude: Double, longitude: Double, timezone: String) -> String {
        "\(weatherPrefix)latitude=\(latitude)&longitude=\(longitude)\(dailySuffix)&timezone=\(timezone)"
    }
    
    static func getLocationEndpoint(location: String) -> String {
        "\(locationPrefix)\(location)\(locationSuffix)"
    }
}
