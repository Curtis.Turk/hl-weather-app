//
//  CombineServiceInteractorMock.swift
//  HL weather app
//
//  Created by Curtis Turk on 22/02/2024.
//

import Foundation
import Combine

class CombineServiceInteractorMock: CombineServiceInteractorProtocol {

    func getLocationPublisher(for locationName: String) -> AnyPublisher<LocationModel, Error> {
        
        let locationResultStub = LocationModel.Result.StubFactory.makeLocationResult()
        let locationModelStub = LocationModel(results: [locationResultStub], generationtimeMS:  100.0)
        
        return Just(locationModelStub)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
    
    func getDailyWeatherDataPublisher(latitude: Double, longitude: Double, timezone: String) -> AnyPublisher<WeatherModel, Error> {
        let dailyWeatherStub = WeatherModel.StubFactory.makeDailyWeatherModel()
        
        return Just(dailyWeatherStub)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}
