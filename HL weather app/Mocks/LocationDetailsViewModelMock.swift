//
//  LocationDetailsViewModelMock.swift
//  HL weather app
//
//  Created by Curtis Turk on 16/08/2023.
//

import Foundation

class LocationDetailsViewModelMock: LocationDetailsViewModel {
    
    var isBuilt = false
    
    override init(locationResult: LocationModel.Result) {
        super.init(locationResult: locationResult)
        isBuilt = true
    }
}
