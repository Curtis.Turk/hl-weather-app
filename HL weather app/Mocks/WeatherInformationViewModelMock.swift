//
//  WeatherInformationViewModel.swift
//  HL weather app
//
//  Created by Curtis Turk on 16/08/2023.
//

import Foundation

class WeatherInformationViewModelMock: WeatherInformationViewModel {
    
    var isBuilt = false
    
    override init(weatherData: WeatherModel) {
        super.init(weatherData: weatherData)
        isBuilt = true
    }
}
