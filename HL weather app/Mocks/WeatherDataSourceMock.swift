//
//  MockWeatherDataSource.swift
//  HL weather app
//
//  Created by Curtis Turk on 16/08/2023.
//

import Foundation

class WeatherDataSourceMock: WeatherDataSourceProtocol {
    
    let mockWeatherData = WeatherData(
        weatherData: WeatherModel.StubFactory.makeDailyWeatherModel(),
        locationData: LocationModel(
            results: [LocationModel.Result.StubFactory.makeLocationResult()],
            generationtimeMS: 1.00)
    )
    
    func fetchUserLocationData(schedule: ScheduleSelection, completion: @escaping (Result<WeatherData, Error>) -> Void) async {
        let mockLocation = "Tokyo"
        
        fetchMockData(for: mockLocation, schedule: schedule){ result in
            completion(result)
        }
    }
    
    func fetchUserSearchLocationData(for locationToSearch: String, schedule: ScheduleSelection, completion: @escaping (Result<WeatherData, Error>) -> Void) {
        
        fetchMockData(for: locationToSearch, schedule: schedule) { result in
            completion(result)
        }
    }
    
    func fetchMockData(
        for location: String,
        schedule: ScheduleSelection = ScheduleSelection.daily,
        completion: @escaping (Result<WeatherData, Error>) -> Void
    ){
        completion(.success(mockWeatherData))
    }
}
