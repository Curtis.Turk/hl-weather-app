//
//  HL_weather_appUITests.swift
//  HL weather appUITests
//
//  Created by Curtis Turk on 17/07/2023.
//

import XCTest

final class HL_weather_appUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testLookAtHourlyWeatherOnCurrentTab() throws {
        let app = XCUIApplication()
        app.launch()
        let hourlySelector = XCUIApplication()/*@START_MENU_TOKEN@*/.buttons["Hourly"]/*[[".segmentedControls.buttons[\"Hourly\"]",".buttons[\"Hourly\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(hourlySelector.exists)
            hourlySelector.tap()
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
