//
//  TodayWidget.swift
//  TodayWidget
//
//  Created by Curtis Turk on 10/05/2024.
//

import WidgetKit
import SwiftUI

struct Provider: AppIntentTimelineProvider {
    func placeholder(in context: Context) -> WeatherEntry {
        WeatherEntry(
            date: Date(),
            configuration: ConfigurationAppIntent(),
            hourWeather: HourWeather.Factory.makeHourWeather()
        )
    }
    
    func snapshot(for configuration: ConfigurationAppIntent, in context: Context) async -> WeatherEntry {
        WeatherEntry(
            date: Date(),
            configuration: ConfigurationAppIntent(),
            hourWeather: HourWeather.Factory.makeHourWeather()
        )
    }
    
    
    func timeline(for configuration: ConfigurationAppIntent, in context: Context) async -> Timeline<WeatherEntry> {
        var entries: [WeatherEntry] = []
        
        let currentDate = Date()
        
        var weatherModel: WeatherModel
        do {
            weatherModel = try await ServiceInteractor.shared.getHourlyWeatherData(latitude: 51.45523, longitude: -2.59665, timezone: "Europe/London")
        } catch {
            weatherModel = WeatherModel.StubFactory.makeHourlyWeatherModel()
        }
        
        func getCurrentDateIndex() -> Int {
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: currentDate)
            return hour
        }
        
        if let hourlyWeather = weatherModel.hourly {
            let index = getCurrentDateIndex()
            
            if index < hourlyWeather.time.count {
                let hourWeather = HourWeather(
                    time: hourlyWeather.time[index],
                    temperature2m: hourlyWeather.temperature2m[index],
                    precipitationProbability: hourlyWeather.precipitationProbability[index],
                    precipitation: hourlyWeather.precipitation[index],
                    windspeed10m: hourlyWeather.windspeed10m[index],
                    winddirection10m: hourlyWeather.winddirection10m[index],
                    weathercode: hourlyWeather.weathercode[index]
                )
                
                entries.append(
                    WeatherEntry(
                        date: currentDate,
                        configuration: ConfigurationAppIntent(),
                        hourWeather: hourWeather
                    )
                )
            }
        }
        
        return Timeline(entries: entries, policy: .after(Date().addingTimeInterval(3600)))
    }
}

struct HourWeather {
    let time: String
    let temperature2m: Double
    let precipitationProbability: Int?
    let precipitation: Double
    let windspeed10m: Double
    let winddirection10m: Int
    let weathercode: Int
    
    struct Factory {
        static func makeHourWeather() -> HourWeather {
            HourWeather(
                time: "2023-08-06T09:00",
                temperature2m: 13.5,
                precipitationProbability: 19,
                precipitation: 1.70,
                windspeed10m: 3.3,
                winddirection10m: 276,
                weathercode: 3
            )
        }
    }
}

struct WeatherEntry: TimelineEntry {
    var date: Date
    let configuration: ConfigurationAppIntent
    let hourWeather: HourWeather
}

struct TodayWidgetEntryView : View {
    var entry: Provider.Entry
    
    var hour: String {
        getHour(for: entry.hourWeather.time)
    }
    var iconName: String {
        weathercodeIcons[entry.hourWeather.weathercode] ?? ""
    }
    var temperature: String {
        entry.hourWeather.temperature2m.description
    }
    var body: some View {
        ZStack {
            Color.teal
            VStack {
                Text(hour)
                    .font(.title3)
                
                Image(systemName: iconName)
                    .symbolRenderingMode(.multicolor)
                    .font(.system(size: 60))
                
                HStack(alignment: .top, spacing: .zero) {
                    Text(temperature)
                        .font(.title2)
                    
                    Text("°C")
                        .font(.caption)
                }
            }
        }
    }
}

struct TodayWidget: Widget {
    let kind: String = "TodayWidget"
    
    var body: some WidgetConfiguration {
        AppIntentConfiguration(kind: kind, intent: ConfigurationAppIntent.self, provider: Provider()) { entry in
            TodayWidgetEntryView(entry: entry)
                .containerBackground(.fill.tertiary, for: .widget)
        }
        .contentMarginsDisabled()
    }
}

extension ConfigurationAppIntent {
    fileprivate static var smiley: ConfigurationAppIntent {
        let intent = ConfigurationAppIntent()
        intent.favoriteEmoji = "😀"
        return intent
    }
    
    fileprivate static var starEyes: ConfigurationAppIntent {
        let intent = ConfigurationAppIntent()
        intent.favoriteEmoji = "🤩"
        return intent
    }
}

#Preview(as: .systemSmall) {
    TodayWidget()
} timeline: {
    WeatherEntry(
        date: .now,
        configuration: .smiley,
        hourWeather: HourWeather.Factory.makeHourWeather()
    )
}
