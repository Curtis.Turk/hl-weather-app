//
//  TodayWidgetBundle.swift
//  TodayWidget
//
//  Created by Curtis Turk on 10/05/2024.
//

import WidgetKit
import SwiftUI

@main
struct TodayWidgetBundle: WidgetBundle {
    var body: some Widget {
        TodayWidget()
    }
}
