# Weather App

## Hargreaves Landsdown / Swift

### Built for learning and development using:

- MVVM architechture
- Coordinator pattern
- UIKit for the initial view and tabBar
- SwiftUI for the additional views
- Unit testing for View Models
- open-meteo.com API for weather data and geocoding

### Features:

- View weather for your current location
- Search cities globally for weather at chosen location
- View details about the location including latitude and longitude
- Choose forecasts in temperature, precipitation and wind details
- Choose to view a weekly view to see upcoming weather at locations
- Choose to view an hourly view to see the 24 hours of weather

# Screenshots
Current Tab            |  Search Tab
:-------------------------:|:-------------------------:
![Current](/Screenshots/CurrentTab.png "Current") | ![Search](/Screenshots/SearchTab.png "Search")
