//
//  DateFormatterTests.swift
//  HL weather appTests
//
//  Created by Curtis Turk on 15/08/2023.
//

import XCTest
@testable import HL_weather_app

final class DateFormatterTests: XCTestCase {

    func testExtractCurrentHours_returnsCorrectValuesFromDates() {
        //given

        let hours = ["2023-07-31T00:00","2023-07-31T01:00","2023-07-31T02:00","2023-08-05T13:00"]
        let dateFormatter = hourlyDateFormatter()
        
        let mockDate = dateFormatter.date(from: "2023-07-31T00:00")!
        let mockDateProvider = MockDateProvider(currentDate: mockDate)
        
        // When
        let result = extractCurrentHours(from: hours, numberOfDays: 1, dateProvider: mockDateProvider)
        let expectation = ["00","01","02"]
        
        // Then
        XCTAssertEqual(result, expectation)
    }
    func testExtractCurrentHours_returnsExpectedString() {
        
        //given
        let hours = ["2023-07-31T00:00","2023-07-31T01:00","2023-07-31T02:00","2023-08-05T13:00"]
        let dateFormatter = hourlyDateFormatter()
        
        let mockDate = dateFormatter.date(from: "2023-08-05T00:00")!
        let mockDateProvider = MockDateProvider(currentDate: mockDate)
        
        // When
        let result = extractCurrentHours(from: hours, numberOfDays: 1, dateProvider: mockDateProvider)
        let expectation = ["13"]
        
        // Then
        XCTAssertEqual(result, expectation)
    }
    func testExtractCurrentHours_returnsEmptyWhenGivenCurrentDate() {
        
        //given
        let hours = ["2023-07-31T00:00","2023-07-31T01:00","2023-07-31T02:00","2023-08-05T13:00"]
        
        let mockDate = Date()
        let mockDateProvider = MockDateProvider(currentDate: mockDate)
        
        // When
        let result = extractCurrentHours(from: hours, numberOfDays: 1, dateProvider: mockDateProvider)
        let expectation: [String]? = []
        
        // Then
        XCTAssertEqual(result, expectation)
    }
    
    func testGetWeekday(){

        //given
        let testDate = "2023-08-15"
        
        //when
        let result = getWeekday(for: testDate)
        let expectation = "Tue"
        
        // Then
        XCTAssertEqual(result, expectation)
    }
    
    func testGetHour(){
        //given
        let testDate = "2023-07-31T12:00"
        
        //when
        let result = getHour(for: testDate)
        let expectation = "12"
        
        // Then
        XCTAssertEqual(result, expectation)
    }
}
