//
//  WeatherInformationViewModelTests.swift
//  HL weather appTests
//
//  Created by Kyle Dold on 11/08/2023.
//

import XCTest
import Combine
@testable import HL_weather_app

final class WeatherInformationViewModelTests: XCTestCase {
    
    private let sut = WeatherInformationViewModel(
        weatherData: WeatherModel.StubFactory.makeDailyWeatherModel()
    )
    
    var cancellables = [AnyCancellable]()
    
    func test_whenForecaseSelectionChanged_thenCorrectValuePublished() async {
        // Given
        let expectation = XCTestExpectation(description: "Publishes forecast")
        
        sut.$forecastSelection
            .dropFirst()
            .sink(receiveValue: {
                XCTAssertEqual($0, .rain)
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        // When
        sut.forecastSelection = .rain
        
        // Then
        await fulfillment(of: [expectation], timeout: 1)
    }
}
