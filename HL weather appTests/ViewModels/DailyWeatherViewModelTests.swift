//
//  DailyWeatherViewModelTests.swift
//  HL weather appTests
//
//  Created by Curtis Turk on 14/08/2023.
//

import XCTest
@testable import HL_weather_app

final class DailyWeatherViewModelTests: XCTestCase {

    private let sut = DailyWeatherViewModel(
        dailyWeather: WeatherModel.StubFactory.makeDailyWeather(),
        dailyUnits: WeatherModel.StubFactory.makeDailyWeatherUnits(),
        forecastSelection: ForecastSelection.temp)
    
    func test_whenIsTodayCalled_thenReturnsExpectedBool() {
        // Today is the first index in array
        let resultTrue = sut.isToday(0)
        let resultFalse = sut.isToday(1)
        
        XCTAssertTrue(resultTrue)
        XCTAssertFalse(resultFalse)
    }
}
