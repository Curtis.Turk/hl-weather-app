//
//  CurrentTabViewModelTests.swift
//  HL weather appTests
//
//  Created by Curtis Turk on 15/08/2023.
//

import XCTest
import Combine
@testable import HL_weather_app

final class CurrentTabViewModelTests: XCTestCase {

    private var sut: CurrentTabViewModel = CurrentTabViewModel(
        dataSource: WeatherDataSourceMock(),
        combineServiceInteractor: CombineServiceInteractorMock(),
        locationDetailsViewModel: nil,
        weatherInformationViewModel: nil
    )
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = CurrentTabViewModel(
            dataSource: WeatherDataSourceMock(),
            locationDetailsViewModel: nil,
            weatherInformationViewModel: nil
        )
        
        super.tearDown()
    }
    
    func test_buildsLocationandWeatherViewModels_whenViewAppearedItsCalled() async {
        let expectation = XCTestExpectation(description: "ViewModels are built")
        
        await sut.viewAppeared(schedule: ScheduleSelection.daily)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            XCTAssertTrue((self.sut.locationDetailsViewModel?.latitude) != nil)
            XCTAssertTrue((self.sut.weatherInformationViewModel?.weatherData.latitude) != nil)
            
            expectation.fulfill()
        }
        
        await fulfillment(of: [expectation],timeout: 2.0)
    }
    
    func test_buildsLocationPublishers_whenViewModelCreated() async {
        let expectation = XCTestExpectation(description: "Location publisher is built")
        
        sut.fetchLocationDataUsingPublishers()
    
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            XCTAssertNotNil(self.sut.location, "Location should be fetched")
            print(self.sut.location ?? "no location")
            XCTAssertEqual(self.sut.location?.latitude, 51.45523)
            XCTAssertEqual(self.sut.location?.name, "Bristol")
            expectation.fulfill()
            
        }
        await fulfillment(of: [expectation],timeout: 2.0)
    }
    
    func test_buildsWeatherPublishers_whenViewModelCreated() async {
        let expectation = XCTestExpectation(description: "Weather publisher is built")
        
        sut.fetchLocationDataUsingPublishers()
        sut.fetchWeatherDataBasedOnLocation()
    
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            XCTAssertNotNil(self.sut.dailyWeather, "Weather should be fetched")
            XCTAssertEqual(self.sut.dailyWeather?.daily?.temperature2mMin, [12.1, 12.0, 15.7, 16.4, 16.0, 15.6, 15.6])
            XCTAssertEqual(self.sut.dailyWeather?.daily?.precipitationSum, [6.6, 2.4, 5.5, 6.2, 0.0, 0.0, 9.0])
            print(self.sut.dailyWeather ?? "no weather")
            expectation.fulfill()
        }
        await fulfillment(of: [expectation],timeout: 2.0)
    }
}
