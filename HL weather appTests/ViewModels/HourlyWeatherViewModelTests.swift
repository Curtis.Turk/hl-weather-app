//
//  HourlyWeatherViewModelTests.swift
//  HL weather appTests
//
//  Created by Curtis Turk on 14/08/2023.
//

import XCTest
@testable import HL_weather_app

final class HourlyWeatherViewModelTests: XCTestCase {

    private let sut = HourlyWeatherViewModel(
        hourlyWeather: WeatherModel.StubFactory.makeHourlyWeather(
            time: ["2023-07-31T00:00","2023-07-31T01:00","2023-07-31T02:00","2023-08-05T13:00"],
            temperature2m: [17.0, 17.2, 17.0, 17.5],
            precipitationProbability: [2, 0, 10],
            precipitation: [0.02, 0.00, 2.20],
            weathercode: [2,2,2,15]
        ),
        hourlyUnits: WeatherModel.StubFactory.makeHourlyWeatherUnits(),
        forecastSelection: ForecastSelection.temp
    )
    
    func test_whenIsToday_thenReturnsExpectedBool() {
//      Today is the first value
        let resultFalse = sut.isToday(1)
        let resultTrue = sut.isToday(0)
        
        XCTAssertFalse(resultFalse)
        XCTAssertTrue(resultTrue)
    }
    func test_whenIsTodayOrEvery_thenReturnsExpectedBool() {
    
        let resultFalse = sut.isTodayOrEverySixDays(1)
        let resultFalse2 = sut.isTodayOrEverySixDays(6)
        
        let resultTrue = sut.isTodayOrEverySixDays(0)
        let resultTrue2 = sut.isTodayOrEverySixDays(5)
        
        XCTAssertFalse(resultFalse)
        XCTAssertFalse(resultFalse2)
        
        XCTAssertTrue(resultTrue)
        XCTAssertTrue(resultTrue2)
    }
    func test_whenIsTomorrow_thenReturnsExpectedBool() {
//      Tomorrow always starts a midnight 00
        let resultFalse = sut.isTomorrow("01")
        let resultFalse2 = sut.isTomorrow("test")
        
        let resultTrue = sut.isTomorrow("00")
        
        XCTAssertFalse(resultFalse)
        XCTAssertFalse(resultFalse2)
        XCTAssertTrue(resultTrue)
    }
    func test_whenWeatherCodeIconTextsCall_thenReturnsExpectedArray(){
        // Given
        // When
        let results = sut.weatherCodeIconTexts
        // 2 = cloud.sun / 15 = weathercode not found default used instead
        let expectation = ["cloud.sun","cloud.sun","cloud.sun","sun.max"]
        // Then
        XCTAssertEqual(results, expectation)
    }
    func test_whenHoursIsCalled_thenReturnsExpectedArray(){
        // Given
        // When
        let results = sut.hours
        
        // current date not matching with given dates
        let expectation: [String] = []
        
        // Then
        XCTAssertEqual(results, expectation)
    }
    
    func test_temperatureComputedValue(){
        // Given
        // When
        let results = sut.temperature
        
        let expectation = [17.0, 17.2, 17.0, 17.5]
                
        // Then
        XCTAssertEqual(results, expectation)
    }
    
    func test_precipitationComputedValue(){
        // Given
        // When
        let results = sut.precipitation
        
        let expectation = [0.02, 0.00, 2.20]
                
        // Then
        XCTAssertEqual(results, expectation)
    }
    func test_precipitationProbabliityComputedValue(){
        // Given
        // When
        let results = sut.precipitationProbability
        
        let expectation = [2, 0, 10]
                
        // Then
        XCTAssertEqual(results, expectation)
    }
}
