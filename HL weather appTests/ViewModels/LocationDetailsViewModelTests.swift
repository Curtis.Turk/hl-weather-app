//
//  LocationDetailsViewModelTest.swift
//  HL weather appTests
//
//  Created by Kyle Dold on 11/08/2023.
//

import XCTest
@testable import HL_weather_app

final class LocationDetailsViewModelTest: XCTestCase {
    
    private let sut = LocationDetailsViewModel(
        locationResult: LocationModel.Result.StubFactory.makeLocationResult(
            name: "Bath",
            latitude: 50.002,
            longitude: -50.002,
            countryCode: "GB",
            timezone: "GMT",
            admin1: "England"
        )
    )
    
    func test_whenNameCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.name
        
        //then
        XCTAssertEqual(result, "Bath")
    }
    
    func test_whenCountryCodeCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.countryCode
        
        //then
        XCTAssertEqual(result, "GB")
    }
    func test_whenAdminCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.admin
        
        //then
        XCTAssertEqual(result, "England")
    }
    func test_whenTimezoneCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.timezone
        
        //then
        XCTAssertEqual(result, "GMT")
    }
    func test_whenLatitudeCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.latitude
        
        //then
        XCTAssertEqual(result, "50.002")
    }
    func test_whenLongitudeCalled_thenReturnsExpectedString() {
        //given
        //when
        let result = sut.longitude
        
        //then
        XCTAssertEqual(result, "-50.002")
    }
}
